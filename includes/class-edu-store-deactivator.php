<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://edustore.online
 * @since      1.0.0
 *
 * @package    Edu_Store
 * @subpackage Edu_Store/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Edu_Store
 * @subpackage Edu_Store/includes
 * @author     EduStore <contato@edustore.online>
 */
class Edu_Store_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
