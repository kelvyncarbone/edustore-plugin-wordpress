<?php

/**
 * Fired during plugin activation
 *
 * @link       https://edustore.online
 * @since      1.0.0
 *
 * @package    Edu_Store
 * @subpackage Edu_Store/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Edu_Store
 * @subpackage Edu_Store/includes
 * @author     EduStore <contato@edustore.online>
 */
class Edu_Store_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
