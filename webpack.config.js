
const path = require('path')
const defaults = require('@wordpress/scripts/config/webpack.config');

module.exports = {
    ...defaults,
    externals: {
        react: 'React',
        'react-dom': 'ReactDOM',
    },
    output: {
        filename: 'edu-store.js',
        path: path.join(__dirname, 'assets/js'),
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '...'],

    },
};

