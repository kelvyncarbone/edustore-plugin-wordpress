import React, { useEffect, useMemo, useRef, useState } from 'react';
import { API } from '../../Request/Api';
import Loader from "../../Components/Loaded";
import { TestimonialProps } from '../../@types/testimonials';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper/modules';
import { FaStar } from "react-icons/fa6";

import 'swiper/css';
import 'swiper/css/effect-fade';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

import "../../base.css";
import { Quotes } from '../../Components/Quotes';

const Testimonials: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [data, setData] = useState<TestimonialProps[]>([]);

    const [atts, setAtts] = useState<{ config: { per_page: number, title: string } }>({ config: { per_page: 10, title: 'Depoimentos' } });

    const handleRequestData = (route: string = '/faq', page?: number) => {
        setIsLoading(true);

        API.get(route, {}, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    setData(res.data);
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData('/testimonials')
        const atts = ((document.getElementById('edustore_testimonials_atts') ?? document.getElementById('edustore_depoimentos_atts')) as HTMLInputElement)
        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)
                atts.remove()

                setAtts(data);
            }
            catch (e) {
                console.error(e);
            }
        }
    }, []);

    return (
        isLoading ? <Loader /> :
            <div className="space-y-4">
                <section className="edu_testimonials_section">
                    <div className="edu_testimonials_container">
                        <div className="edu_testimonials_slides_container">
                            <div className="edu_testimonials_slides">
                                <Swiper
                                    spaceBetween={8}
                                    slidesPerView={"auto"}
                                    navigation={true}
                                    pagination={{
                                        clickable: true,
                                    }}
                                    modules={[Navigation, Pagination]}
                                    className="edu_swiper p-0.5"
                                > {
                                        data.map((testimonial) => <SwiperSlide key={testimonial.id} >
                                            <div className="bg-[rgba(255,255,255,.1)] border p-12 rounded-2xl relative">
                                                <Quotes />
                                                <div className="flex mb-8 items-center">
                                                    {testimonial.avatar && <img
                                                        src={testimonial.avatar}
                                                        className="h-auto w-16 w-max-[100%] mr-4 rounded"
                                                        width="60"
                                                        alt="user"
                                                    />
                                                    }
                                                    <div className="author-info">
                                                        <h6 className="mb-0">{testimonial.testimonial_name}</h6>
                                                        <small>{testimonial.testimonial_office}</small>
                                                    </div>
                                                </div>
                                                <blockquote>
                                                    <div
                                                        dangerouslySetInnerHTML={{ __html: testimonial.testimonial_description }}
                                                    />
                                                </blockquote>
                                                <div className='mt-2 w-full flex items-center'>
                                                    <FaStar color='#ffc300' className='mr-2' />
                                                    <FaStar color='#ffc300' className='mr-2' />
                                                    <FaStar color='#ffc300' className='mr-2' />
                                                    <FaStar color='#ffc300' className='mr-2' />
                                                    <FaStar color='#ffc300' className='mr-2' />
                                                </div>
                                            </div>

                                        </SwiperSlide>
                                        )
                                    }
                                </Swiper>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    );
};

export { Testimonials };
