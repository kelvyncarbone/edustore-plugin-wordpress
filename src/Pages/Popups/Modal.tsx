import React from 'react';

interface ModalProps {
    isOpen: boolean;
    onClose: () => void;
    children: React.ReactNode;
}

const Modal: React.FC<ModalProps> = ({ isOpen, onClose, children }) => {
    return (
        <>
            {isOpen && (
                <div className="fixed inset-0 z-50 flex items-center justify-center">
                    <div className="absolute inset-0 bg-black opacity-50" onClick={onClose}></div>
                    <div className="bg-transparent relative w-full h-full max-w-3xl p-8 rounded z-20">{children}</div>
                </div>
            )}
        </>
    );
};

export { Modal };
