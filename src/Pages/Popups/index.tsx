import React, { useEffect, useState } from 'react';
import { API } from '../../Request/Api';
import { PopupProps } from "../../@types/popup";

import "../../base.css";;
import { Modal } from './Modal';
import { Carousel } from 'flowbite-react';
import { AiFillCloseCircle } from 'react-icons/ai';

function getCookieValue(name: string) {
    const regex = new RegExp(`(^| )${name}=([^;]+)`)
    const match = document.cookie.match(regex)
    if (match) {
        return match[2]
    }
}
const Popups: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isOpen, setIsOpen] = useState<boolean>(true);

    const [data, setData] = useState<PopupProps[]>([]);

    const [atts, setAtts] = useState<{ config: { timeinterval: number, frequency: number } }>({ config: { timeinterval: 5000, frequency: 1 } });


    const handleRequestData = (route: string = '/faq', page?: number) => {
        setIsLoading(true);

        API.get(route, {}, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    setData(res.data);
                }
            });
        })
    }


    useEffect(() => {
        const atts = (document.getElementById('edustore_popups_atts') as HTMLInputElement)
        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)
                setAtts(data);

                atts.remove()
            }
            catch (e) {
            }
        }
    }, []);

    useEffect(() => {
        if (atts) {
            if (atts.config.frequency !== -1 && getCookieValue('edustore_show_popup_count'))
                return;

            handleRequestData('/popups');
        }
    }, [atts]);

    const handleClose = () => {
        setIsOpen(false)
        API.post('/popup-view');
    }

    return (
        isLoading ? <></> :
            (data.length === 0 ? <></> : <Modal isOpen={isOpen} onClose={handleClose}>
                <span className='absolute cursor-pointer top-10 right-10 z-30' onClick={() => handleClose()}><AiFillCloseCircle className='hover:text-[--edustore-secondary-color]' size={45} /></span>
                <Carousel slideInterval={atts.config.timeinterval} pauseOnHover>
                    {data.map((popup) =>
                        <div className='edu_popup_type_img_container' key={popup.id}>
                            {[
                                <img
                                    alt={"popup image"}
                                    src={popup.image}
                                    className="edu_popup_type_img"
                                />,
                                <div className='edu_popup_type_video_container' dangerouslySetInnerHTML={{ __html: popup.popup_content ?? '' }} />,
                            ][Number(popup.popup_type) - 1]
                            }

                        </div>
                    )
                    }
                </Carousel>
            </Modal>
            )
    )
};

export { Popups };
