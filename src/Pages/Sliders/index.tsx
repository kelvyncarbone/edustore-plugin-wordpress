import React, { useEffect, useState } from 'react';
import { API } from '../../Request/Api';
import Loader from "../../Components/Loaded";
import { SlideProps } from "../../@types/slide";
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectFade, Navigation, Pagination } from 'swiper/modules';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/effect-fade';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

import "../../base.css";;

const Sliders: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [data, setData] = useState<SlideProps[]>([]);

    const [atts, setAtts] = useState<{ config: { per_page: number } }>({ config: { per_page: 10 } });

    const handleRequestData = (route: string = '/faq', page?: number) => {
        setIsLoading(true);

        API.get(route, {}, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    setData(res.data);
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData('/sliders')

        const atts = (document.getElementById('edustore_sliders_atts') as HTMLInputElement)
        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)

                atts.remove()

                setAtts(data);
            }
            catch (e) {
                console.error(e);
            }
        }
    }, []);

    return (
        isLoading ? <Loader /> :
            <>
                <Swiper
                    spaceBetween={30}
                    effect={'fade'}
                    navigation={true}
                    pagination={{
                        clickable: true,
                    }}
                    modules={[EffectFade, Navigation, Pagination]}
                    className="edu_swiper"
                >{
                        data.map((slide) =>
                            <SwiperSlide className='edu_swiper_slide' key={slide.id}>
                                <img className='edu_swiper_img' src={slide.src} />
                                <div className='edu_swiper_title_container'>
                                    <h2 className='edu_swiper_title'>{slide.title}</h2>
                                </div>
                            </SwiperSlide>
                        )}
                </Swiper>
            </>
    );
};

export { Sliders };
