import React, { useEffect, useMemo, useRef, useState } from 'react';
import { API } from '../../Request/Api';
import Loader from "../../Components/Loaded";
import { BlogPostProps } from "../../@types/blogPost";

import "../../base.css";;

const Blog: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [data, setData] = useState<BlogPostProps[]>([]);

    const [atts, setAtts] = useState<{ config: { per_page: number } }>({ config: { per_page: 10 } });

    const handleRequestData = (route: string = '/faq', page?: number) => {
        setIsLoading(true);

        API.get(route, {}, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    setData(res.data);
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData('/blog')

        const atts = (document.getElementById('edustore_blog_atts') as HTMLInputElement);

        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)

                atts.remove();

                setAtts(data);
            }
            catch (e) {
                console.error(e);
            }
        }
    }, []);

    return (
        isLoading ? <Loader /> :
            <div className="flex flex-wrap">
                {data.map((post) => {
                    return <div key={post.id} className='edu_blog_container'>
                        <article className="edu_blog_item">
                            <a className='edu_blog_item_img_container' href={post.url_post} >
                                <img
                                    alt={post.title}
                                    src={post.card_image}
                                    className="edu_blog_item_img"
                                />

                            </a>
                            <div className="edu_blog_item_content_container">
                                <time dateTime={post.published_at} className="block text-xs text-gray-500"> {post.published_at} </time>

                                <a href={post.url_post} >
                                    <h2 className="edu_blog_item_title">{post.title}</h2>
                                </a>

                                {post.subtitle && <p className="edu_blog_item_description">
                                    {post.subtitle}
                                </p>
                                }
                            </div>
                        </article>
                    </div>

                })}
            </div>
    );
};

export { Blog };
