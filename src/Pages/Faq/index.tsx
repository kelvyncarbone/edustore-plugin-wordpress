import React, { useEffect, useState } from 'react';
import { API } from '../../Request/Api';
import Loader from "../../Components/Loaded";

import "../../base.css";;
import 'react-responsive-pagination/themes/classic.css';
import { faqProps } from '../../@types/faq';

const Faq: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [data, setData] = useState<faqProps[]>([]);
    const [atts, setAtts] = useState<{ config: { per_page: number, showtitle: string } }>({ config: { per_page: 10, showtitle: 'y' } });

    const handleRequestData = (route: string = '/faq', page?: number) => {
        setIsLoading(true);

        API.get(route, {}, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    setData(res.data);
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData('/faq')

        const atts = (document.getElementById('edustore_faq_atts') as HTMLInputElement)
        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)

                atts.remove();

                setAtts(data);
            }
            catch (e) {
                console.error(e);
            }
        }
    }, []);

    return (
        isLoading ? <Loader /> :
            <div className="space-y-4">
                {
                    data.map((faq_item, index) => <div key={"faq_section" + index} className='edu_faq_section'>
                        {atts.config.showtitle === 'y' ? <div className='edu_faq_section_title_container'><h2 className='edu_faq_section_title'>{faq_item.title}</h2></div> : <></>}
                        <div className='edu_faq_section_content'>
                            {faq_item.faq_sections.map((faq, index) => <details
                                key={faq.id + '_faq'}
                                className="edu_faq_container"
                                open={index === 0}
                            >
                                <summary className="edu_faq_tab">
                                    <h2 className="edu_faq_title">
                                        {faq.title}
                                    </h2>

                                    <span className="shrink-0 rounded-full bg-white p-1.5 text-gray-900 sm:p-3">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            className="h-5 w-5 shrink-0 transition duration-300 group-open:-rotate-45"
                                            viewBox="0 0 20 20"
                                            fill="currentColor"
                                        >
                                            <path
                                                fillRule="evenodd"
                                                d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                                                clipRule="evenodd"
                                            />
                                        </svg>
                                    </span>
                                </summary>

                                <div dangerouslySetInnerHTML={{ __html: faq.content }} className="edu_faq_content" />

                            </details>
                            )}
                        </div>

                    </div>)

                }
            </div>
    );
};

export { Faq };
