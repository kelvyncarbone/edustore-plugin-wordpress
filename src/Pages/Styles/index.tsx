import React, { useEffect, useState } from 'react';
import { Button, ToggleSwitch } from 'flowbite-react';
import { API } from '../../Request/Api';
//@ts-ignore
import { ToastContainer, toast } from 'react-toastify';
import Loader from '../../Components/Loaded';
import CodeEditor from '@uiw/react-textarea-code-editor';

import "./index.css";

const Styles: React.FC = () => {
    const [code, setCode] = useState('');
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isSaving, setIsSaving] = useState<boolean>(false);
    const [useCustom, setUserCustom] = useState(false);

    const handleRequestData = (type?: string) => {
        setIsLoading(true);

        API.get('/styles', { type }, (response) => {
            response.json().then((res) => {
                setIsLoading(false)

                if (res.status === 'ok') {
                    setCode(res.data);
                    setUserCustom(res.isCustom);
                }
            });
        })
    }

    const handleSaveData = (type?: string) => {
        setIsSaving(true);

        API.post('/styles', { isCustom: useCustom, code }, (response) => {
            response.json().then((res) => {
                setIsSaving(false)

                if (res.status === 'ok') {
                    toast(res.message, {
                        type: 'success',
                        position: 'bottom-center'
                    })
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData('custom');
    }, [])


    return (
        <div className='bg-white flex flex-col w-full min-h-screen p-2'>
            <div className='w-full my-4'><h3 className='text-xl font-bold'>Edite o estilo padrão dos Shortcodes</h3></div>
            <div className='flex'>
                <div className='sm:w-2/3'>
                    <CodeEditor
                        value={code}
                        language="css"
                        placeholder="Digite o seu css customizado"
                        onChange={(evn) => setCode(evn.target.value)}
                        padding={15}
                        style={{
                            backgroundColor: "#f5f5f5",
                            fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                        }}
                    />
                </div>
                <div className='w-full flex flex-col sm:right-0 sm:fixed sm:w-[27%]  h-full'>
                    <Button isProcessing={isLoading} onClick={() => handleRequestData('default')} className="bg-gray-900 text-white w-[90%] uppercase font-semibold mb-4">Carregar Estilo Padrão</Button>
                    <div className='my-2 border-t h-1 w-[90%]'></div>
                    <span className="w-[90%] flex flex-col my-4">
                        <ToggleSwitch checked={useCustom} label="Ativar Estilo Personalizado" onChange={setUserCustom} />
                    </span>
                    <Button isProcessing={isSaving} onClick={() => handleSaveData()} className="w-[90%] uppercase font-semibold my-4">Salvar</Button>
                </div>
            </div>
            <ToastContainer />
        </div>
    );
}

export { Styles };
