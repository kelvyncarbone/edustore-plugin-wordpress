import React, { useEffect, useState } from 'react';
import { Button, Checkbox, CustomFlowbiteTheme, Label, Select, TextInput, ToggleSwitch } from 'flowbite-react';
import { HexColorInput, HexColorPicker } from "react-colorful";
import { PopoverPicker } from '../../Components/PopoverPicker';
import { API } from '../../Request/Api';
//@ts-ignore
import { ToastContainer, toast } from 'react-toastify';
import Loader from '../../Components/Loaded';
import { CopyToClip } from '../../Components/CopyToClip';

import 'react-toastify/dist/ReactToastify.css';
import "./index.css";

interface EduStoreConfigFormProps {
    onSubmit: (formData: FormData) => void;
}

interface FormData {
    usuario: string;
    senha: string;
    urlVitrine: string;
    urlPainel: string;
    urlAPI: string;
    urlSala: string;
    corPrimaria: string;
    corSecundaria: string;
    popup: boolean;
    popupFrequency: number,
    popupTimeInterval: number
}

const inputReadOnlyTheme: CustomFlowbiteTheme["textInput"] = {
    "field": {
        "rightIcon": {
            "base": "absolute inset-y-0 right-0 flex items-center pr-3",
            "svg": "h-5 w-5 text-gray-500 dark:text-gray-400"
        },

    }
}
const Admin: React.FC = () => {

    const [formData, setFormData] = useState<FormData>({
        usuario: '',
        senha: '',
        urlVitrine: '',
        urlPainel: '',
        urlSala: '',
        urlAPI: '',
        corPrimaria: '#ffffff',
        corSecundaria: '#ffffff',
        popup: false,
        popupFrequency: 1,
        popupTimeInterval: 5000
    });

    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [initialLoad, setInitialLoad] = useState<boolean>(true)

    const handleRequestData = (page?: number) => {

        API.get('/config', {}, (response) => {
            response.json().then((res) => {
                setInitialLoad(false);

                if (res.status === 'ok') {
                    setFormData(res.data);
                }
                else {
                    toast(<div dangerouslySetInnerHTML={{ __html: res.message }} />, {
                        type: 'error',
                        position: 'bottom-center'
                    })
                }
            });
        })
    }

    useEffect(() => {
        handleRequestData();
    }, [])

    const handleColorChange = (color: any, field: 'corPrimaria' | 'corSecundaria') => {
        setFormData((prevData) => ({
            ...prevData,
            [field]: color,
        }));
    };

    const onSubmit = (data) => {
        setIsLoading(true);

        API.post('/config', data, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    toast(res.message, {
                        type: 'success',
                        position: 'bottom-center'
                    })
                }
                else {
                    toast(res.message, {
                        type: 'error',
                        position: 'bottom-center'
                    })
                }
            });
        })
    }

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        onSubmit(formData);
    };

    return (
        <div className='bg-white flex-wrap lg:flex-nowrap w-full flex min-h-screen p-2'>
            <div className='border w-full lg:w-1/2 rounded-xl p-4'>
                <div className='py-4 w-full'>
                    <h2 className='text-2xl font-bold'>EduStore - Configuração</h2>
                </div>
                {
                    initialLoad ? <Loader /> : <form className="w-full flex flex-col gap-4" onSubmit={handleSubmit}>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="usuario" value="Usuário" />
                            </div>
                            <TextInput
                                id="usuario"
                                type="text"
                                placeholder="Informe o usuário"
                                value={formData.usuario}
                                onChange={(e) => setFormData({ ...formData, usuario: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="senha" value="Senha" />
                            </div>
                            <TextInput
                                id="senha"
                                type="password"
                                placeholder="Informe a senha"
                                value={formData.senha}
                                onChange={(e) => setFormData({ ...formData, senha: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="urlAPI" value="URL da API" />
                            </div>
                            <TextInput
                                id="urlAPI"
                                type="url"
                                placeholder="Informe a URL da API"
                                value={formData.urlAPI}
                                onChange={(e) => setFormData({ ...formData, urlAPI: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="urlVitrine" value="URL da vitrine" />
                            </div>
                            <TextInput
                                id="urlVitrine"
                                type="url"
                                placeholder="Informe a URL da vitrine"
                                value={formData.urlVitrine}
                                onChange={(e) => setFormData({ ...formData, urlVitrine: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="urlPainel" value="URL do painel" />
                            </div>
                            <TextInput
                                id="urlPainel"
                                type="url"
                                placeholder="Informe a URL do painel"
                                value={formData.urlPainel}
                                onChange={(e) => setFormData({ ...formData, urlPainel: e.target.value })}
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="urlSala" value="URL da sala" />
                            </div>
                            <TextInput
                                id="urlSala"
                                type="url"
                                placeholder="Informe a URL da sala"
                                value={formData.urlSala}
                                onChange={(e) => setFormData({ ...formData, urlSala: e.target.value })}
                                required
                            />
                        </div>
                        {/* <div>
                            <div className="mb-2 block">
                                <Label htmlFor="urlSala" value="Tempo de Cache da" />
                            </div>
                            <TextInput
                                id="urlSala"
                                type="url"
                                placeholder="Informe a URL da sala"
                                value={formData.urlSala}
                                onChange={(e) => setFormData({ ...formData, urlSala: e.target.value })}
                                required
                            />
                        </div> */}
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="corPrimaria" value="Cor Primária" />
                            </div>
                            <div className='flex items-center'>

                                <PopoverPicker
                                    color={formData.corPrimaria}
                                    onChange={(color) => handleColorChange(color, 'corPrimaria')}
                                />
                                <HexColorInput className='mx-2 rounded-md border p-2 bg-gray-50' color={formData.corPrimaria}
                                    onChange={(color) => handleColorChange(color, 'corPrimaria')} />
                            </div>
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="corSecundaria" value="Cor Secundária" />
                            </div>
                            <div className='flex items-center'>
                                <PopoverPicker
                                    color={formData.corSecundaria}
                                    onChange={(color) => handleColorChange(color, 'corSecundaria')}
                                />
                                <HexColorInput className='mx-2 rounded-md border p-2 bg-gray-50' color={formData.corSecundaria}
                                    onChange={(color) => handleColorChange(color, 'corSecundaria')} />
                            </div>
                        </div>
                        <div className='mb-2'>
                            <ToggleSwitch checked={formData.popup} label="Ativar Popup" onChange={(e) => setFormData({ ...formData, popup: e })} />
                        </div>

                        {formData.popup &&
                            <>
                                <div>
                                    <div className="mb-2 block">
                                        <Label htmlFor="popupFrequency" value="Frequência de Exibição do popup" />
                                    </div>
                                    <div className='flex items-center min-w-[50%]'>
                                        <Select onChange={(e) => setFormData({ ...formData, popupFrequency: Number(e.target.value) })} defaultValue={formData.popupFrequency} name='popupFrequency' id='popupFrequency'>
                                            <option value={1}>Uma vez por dia</option>
                                            <option value={-1}>Sempre</option>
                                        </Select>
                                    </div>
                                </div>
                                <div>
                                    <div className="mb-2 block">
                                        <Label htmlFor="popupTimeInterval" value="Intervalo do slide de cada popup (em segundos)" />
                                    </div>
                                    <div className='flex items-center'>
                                        <TextInput id='popupTimeInterval' type='number' min={0} value={formData.popupTimeInterval / 1000} step={1} onChange={(e) => setFormData({ ...formData, popupTimeInterval: Number(e.target.value) * 1000 })} />
                                    </div>
                                </div>

                            </>
                        }
                        <Button isProcessing={isLoading} type="submit">Salvar</Button>
                    </form>
                }
            </div>
            <div className='border w-full lg:w-1/2 rounded-xl p-4'>
                <div className='py-4 w-full'>
                    <h2 className='text-2xl font-bold'>Shortcodes - Instruções</h2>
                </div>
                {
                    <div className='flex divide-y flex-col w-full'>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Cursos" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_courses theme="cards" show_price="y" per_page="25"]'} rightIcon={CopyToClip} required />
                            <div>
                                <p className='font-[monospace] pt-2'>
                                    <span className='text-base my-4'>OPÇÕES</span>
                                    <br />
                                    <b>theme</b>: "slides" | "cards" ——— Escolha qual o tema que será utilizado para exibir os cursos, padrão: "cards"
                                    <br />
                                    <b>per_page</b>: "número" ——— Escolha a quantidade cursos por página, ex.: per_page="10", per_page="50", padrão: "25"
                                    <br />
                                    <b>show_price</b>: "y" | "n" ——— Escolha se o preço do curso deve aparecer, padrão: "y"
                                    <br />
                                </p>
                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Ebooks" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_ebooks theme="cards" show_price="y" per_page="25"]'} rightIcon={CopyToClip} required />
                            <div>
                                <p className='font-[monospace] pt-2'>
                                    <span className='text-base my-4'>OPÇÕES</span>
                                    <br />
                                    <b>theme</b>: "slides" | "cards" ——— Escolha qual o tema que será utilizado para exibir os ebooks, padrão: "cards"
                                    <br />
                                    <b>per_page</b>: "número" ——— Escolha a quantidade ebooks por página, ex.: per_page="10", per_page="50", padrão: "25"
                                    <br />
                                    <b>show_price</b>: "y" | "n" ——— Escolha se o preço do curso deve aparecer, padrão: "y"
                                    <br />
                                </p>
                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Simulados" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_exams theme="cards" per_page="25"]'} rightIcon={CopyToClip} required />
                            <div>
                                <p className='font-[monospace] pt-2'>
                                    <span className='text-base my-4'>OPÇÕES</span>
                                    <br />
                                    <b>theme</b>: "slides" | "cards" ——— Escolha qual o tema que será utilizado para exibir os simulados, padrão: "cards"
                                    <br />
                                    <b>per_page</b>: "número" ——— Escolha a quantidade simulados por página, ex.: per_page="10", per_page="50", padrão: "25"
                                    <br />
                                </p>
                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar FAQ" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_faq show_title="n"]'} rightIcon={CopyToClip} required />
                            <div>
                                <p className='font-[monospace] pt-2'>
                                    <span className='text-base my-4'>OPÇÕES</span>
                                    <br />
                                    <b>show_title</b>: "y" | "n" ——— Escolha se deve exibir o título do FAQ, padrão: "n"
                                    <br />

                                </p>
                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Posts do Blog" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_blog]'} rightIcon={CopyToClip} required />
                            <div>

                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Sliders" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_sliders]'} rightIcon={CopyToClip} required />
                            <div>

                            </div>
                        </div>
                        <div className="max-w-sm mb-4 pt-2 flex flex-col">
                            <div className="mb-2 block">
                                <Label value="Mostrar Depoimentos" />
                            </div>
                            <TextInput theme={inputReadOnlyTheme} readOnly type="text" value={'[edustore_testimonials title="Depoimentos"]'} rightIcon={CopyToClip} required />
                            <div>
                                <p className='font-[monospace] pt-2'>
                                    <span className='text-base my-4'>OPÇÕES</span>
                                    <br />
                                    <b>title</b>: "Texto" ——— Adicione um titulo para seção, padrão: "Depoimentos"
                                    <br />

                                </p>
                            </div>
                        </div>
                    </div>
                }
            </div>
            <ToastContainer />
        </div>
    );
}

export { Admin };
