import React, { useEffect, useState } from 'react';
import { API } from '../../Request/Api';
import Loader from "../../Components/Loaded";
import { CourseProps } from '../../@types/course';
import { Button, Select, Tabs, TabsRef, TextInput } from 'flowbite-react';
import { AiOutlineSearch } from "react-icons/ai";
import { Categories } from '../../@types/categories';
import ResponsivePagination from 'react-responsive-pagination';
import type { CustomFlowbiteTheme } from 'flowbite-react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Virtual } from 'swiper/modules';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import "../../base.css";;
import 'react-responsive-pagination/themes/classic.css';
import { CourseCard } from '../../Components/CourseCard';
import { ExamCard } from '../../Components/ExamCard';
import { ExamsProps } from '../../@types/exams';
import { Search } from '../../Components/Search';

const tabTheme: CustomFlowbiteTheme["tabs"] = {
    "base": "tab_container",
    "tablist": {
        "base": "flex text-center",
        "styles": {
            "default": "flex-wrap border-b border-gray-200 dark:border-gray-700",
            "underline": "flex-wrap -mb-px border-b border-gray-200 dark:border-gray-700",
            "pills": "flex-wrap font-medium text-sm text-gray-500 dark:text-gray-400 space-x-2",
            "fullWidth": "w-full text-sm font-medium divide-x divide-gray-200 shadow grid grid-flow-col dark:divide-gray-700 dark:text-gray-400 rounded-none"
        },
        "tabitem": {
            "base": "tab_item",
            "styles": {
                "pills": {
                    "base": "",
                    "active": {
                        "on": "tab_item_active",
                        "off": "rounded-lg hover:text-gray-900 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-white"
                    }
                }
            },
        }
    }
}

export type CourseAttsProps = { config: { per_page: number, theme: 'cards' | 'slides', show_price: "y" | "n", assets_url?: string } };

let categoriesTab: { id: number | string, name: string }[] = [];

let CACHED_DATA = {};

let LAST_LOAD_INDEX = 0;

const Exams: React.FC = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [data, setData] = useState<ExamsProps[]>([]);
    const [categories, setCategories] = useState<Categories[]>([]);
    const [currentCategory, setCurrentCategory] = useState<string | number>('all');

    const [searchQuery, setSearchQuery] = useState<string>("");
    const [atts, setAtts] = useState<CourseAttsProps>({ config: { per_page: 25, theme: 'slides', show_price: "y", assets_url: "" } });
    const [startedAtts, setStartedAtts] = useState<boolean>(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [paginate, setPaginate] = useState<{ totalPages: number, totalItems: number }>();

    function handlePageChange(page: number, currentCategory: number | string) {

        handleRequestData('/exams', { per_page: atts.config.per_page, page: page, currentCategory });

        setCurrentPage(prev => page);
    }

    //TODO: create unique handleRequestData for all components
    const handleRequestData = (route: string = '/exams', params?: any, append: boolean = false, callback?: () => void) => {
        const cacheKey = params ? new URLSearchParams(params).toString() : false;

        if (cacheKey && CACHED_DATA[cacheKey]) {
            const { paginate, data } = CACHED_DATA[cacheKey];
            setPaginate(paginate);
            setData(data);

            return;
        }

        if (!append)
            setIsLoading(true);

        API.get(route, params, (response) => {
            response.json().then((res) => {
                setIsLoading(false);

                if (res.status === 'ok') {
                    if (res.categories)
                        setCategories(res.categories.data)

                    if (!append) {
                        setData(res.data);

                        const paginate = { totalItems: res.total, totalPages: res.last_page };
                        setPaginate(paginate);

                        if (cacheKey) {
                            CACHED_DATA[cacheKey] = { data: res.data, paginate };
                        }

                        callback && callback()
                    }
                    else {
                        setData([...data, ...res.data]);
                    }
                }
            });
        })
    }

    useEffect(() => {
        const atts = ((document.getElementById('edustore_exams_atts')) as HTMLInputElement)
        if (atts.value) {
            try {
                const data = JSON.parse(atts.value)
                atts.remove();
                setAtts(data);
                setStartedAtts(true);
            }
            catch (e) {
                console.error(e);
            }
        }
    }, []);

    useEffect(() => {
        if (startedAtts) {
            handleRequestData('/exams', { per_page: atts.config.per_page, page: currentPage, currentCategory });
        }
    }, [startedAtts]);

    const handleSearch = (s: string) => {
        setSearchQuery(s);
    }

    const handleChangeCategory = (category: number | string) => {
        handlePageChange(1, category);
        setCurrentCategory(category)

        setSearchQuery('');
        setCategories(categories.filter((cat) => cat.id !== 'search'))
    }

    const doRequestSearch = () => {
        if (searchQuery.length)
            handleRequestData('/exams', { per_page: atts.config.per_page, page: currentPage, s: searchQuery, currentCategory: 'search' })
        else
            handleRequestData('/exams', { per_page: atts.config.per_page, page: currentPage, currentCategory: 'all' })
    }

    const handleRequestDataWithAppend = (page) => {
        handleRequestData('/exams', { per_page: atts.config.per_page, page, currentCategory }, true);

        setCurrentPage(page);
    }

    return (
        <div className='bg-white w-full min-h-screen p-2'>
            <div className='rounded-xl p-4 relative'>

                {false ? <Loader /> :
                    atts ?
                        <>
                            {isLoading && <div className='absolute z-50 w-full h-full flex items-center justify-center bg-[rgba(255,255,255,0.92)]'><Loader /></div>}
                            {
                                atts.config.theme === 'cards' ?
                                    <>

                                        <div className='mb-2 px-2 w-full flex flex-wrap items-center sm:flex-nowrap'>
                                            <div className='w-full flex-wrap sm:flex-nowrap my-2 sm:my-0 sm:w-[60%] flex items-center'>
                                                <Search placeholder='Pesquisar Simulado' doRequestSearch={doRequestSearch} handleSearch={handleSearch} />
                                            </div>
                                            <div className='w-full my-2 sm:my-0 sm:pl-2 sm:w-[40%] flex justify-end items-center'>
                                                <span className='text-xl'>{paginate ? paginate.totalItems : 0} Simulados</span>
                                            </div>
                                        </div>
                                        <div className="text-sm flex flex-wrap text-gray-500 dark:text-gray-400">
                                            {
                                                data && data.length ?
                                                    data.map((courseItem) => {

                                                        return (
                                                            <ExamCard defaultCover={atts.config.assets_url ? `${atts.config.assets_url}/images/empty-exam.jpeg` : ''} key={courseItem.id} courseItem={courseItem} atts={atts} />
                                                        )
                                                    }) : <div className='text-2xl p-4'>{isLoading ? "" : "Nenhum curso encontrado"}</div>
                                            }
                                        </div>


                                        <div className='w-full my-4'>
                                            {paginate && <ResponsivePagination
                                                total={paginate.totalPages}
                                                current={currentPage}
                                                onPageChange={page => handlePageChange(page, currentCategory)}
                                            />
                                            }
                                        </div>
                                    </>
                                    :
                                    <>

                                        <div className='mb-2 px-2 w-full flex items-center flex-wrap sm:flex-nowrap'>

                                            <div className='w-full flex-wrap sm:flex-nowrap my-2 sm:my-0 sm:pl-2 sm:w-[60%] flex items-center'>
                                                <Search placeholder='Pesquisar Simulado' doRequestSearch={doRequestSearch} handleSearch={handleSearch} />
                                            </div>
                                        </div>
                                        <Swiper
                                            slidesPerView={"auto"}
                                            spaceBetween={4}
                                            navigation
                                            modules={[Navigation, Virtual]}
                                            onSliderMove={(swiper) => {
                                                //is swiping to left
                                                if (swiper.touches.diff >= 0) return;

                                                const size = swiper.slides.length - 1;

                                                if (swiper.activeIndex > LAST_LOAD_INDEX && size < paginate!.totalItems && (swiper.activeIndex % 10 === 0)) {
                                                    handleRequestDataWithAppend(currentPage + 1)
                                                    LAST_LOAD_INDEX = swiper.activeIndex;
                                                }
                                            }}
                                            className="edustore_courses_container"
                                        >
                                            {
                                                data && data.length ? (data).map((courseItem) => {
                                                    return (
                                                        <SwiperSlide key={courseItem.id}>
                                                            <ExamCard defaultCover={atts.config.assets_url ? `${atts.config.assets_url}/images/empty-exam.jpeg` : ''} courseItem={courseItem} classes='z-50 slide' atts={atts} />
                                                        </SwiperSlide>
                                                    )
                                                })
                                                    : <div className='text-2xl p-4'>{isLoading ? "" : "Nenhum curso encontrado"}</div>
                                            }


                                        </Swiper>
                                    </>

                            }
                        </> : <>Ocorreu um erro inesperado ao listar os dados   </>
                }
            </div>

        </div >
    );
};

export { Exams };
