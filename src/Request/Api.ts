// import { Url } from "url";

export class API {
    static baseURL = "";
    static loginNonce: string;
    static assetsURL: string;

    static setLogin(nonce: string) {

        this.loginNonce = nonce;
    }

    static setAssetsURL(url: string) {

        this.assetsURL = url;
    }

    static setBaseURL(baseURL: string) {
        this.baseURL = baseURL;
    }

    static async get(route: string, params?: Object, onSuccess?: (response: Response) => void) {

        const headersProps = {};

        if (this.loginNonce) {
            headersProps['X-WP-Nonce'] = this.loginNonce;
        }

        const headers: RequestInit = {
            headers: new Headers(headersProps)
        }

        const requestURL = new URL(this.baseURL + route);

        if (params) {
            const moreParams = Object.entries(params);

            if (moreParams.length) moreParams.map(([option, value]) => requestURL.searchParams.append(option, value));
        }
        fetch(requestURL, headers).then((response) => {
            if (onSuccess) onSuccess(response);
        });
    }

    static async post(
        route: string,
        data?: Object,
        onSuccess?: (response: Response) => void
    ) {

        const headersProps = {
            'Content-Type': 'application/json',
        };

        if (this.loginNonce) {
            headersProps['X-WP-Nonce'] = this.loginNonce;
        }

        const headers: RequestInit = {
            method: 'POST',
            headers: new Headers(headersProps),
            body: JSON.stringify(data),
        };

        const requestURL = new URL(this.baseURL + route);

        fetch(requestURL.toString(), headers).then((response) => {
            if (onSuccess) onSuccess(response);
        });
    }
}

