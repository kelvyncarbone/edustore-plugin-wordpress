export interface PopupProps {
    id: number;
    image: string;
    url: string;
    type_url: boolean;
    operator_id: number;
    created_at: string;
    updated_at: string;
    published_at: string;
    expiration_at: string;
    deleted_at: string | null;
    local: string;
    popup_type: string;
    popup_size: string;
    popup_content: string | null;
    popup_video: string | null;
    products: any[]; // Adjust this type based on the actual structure of the 'products' property
};