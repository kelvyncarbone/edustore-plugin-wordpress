export interface TestimonialProps {
    id: number;
    testimonial_name: string;
    testimonial_office: string | null;
    testimonial_description: string;
    created_at: string;
    updated_at: string;
    model_type: string;
    model_id: number | null;
    company_name: string | null;
    link: string | null;
    avatar: string | null;
    video_code: string | null;
    cover: string | null;
    deleted_at: string | null;
    courses: any[] | null; // Adjust this type based on the actual structure of the 'courses' property
    state_id: number | null;
    city_id: number | null;
    seal: string | null;
    status: string;
    certification_id: number | null;
    company_id: number | null;
    clean_description: string;
    city: string | null;
    state: string | null;
    institution: string | null;
};
