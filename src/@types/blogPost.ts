export interface BlogPostProps {
    id: number;
    title: string;
    post: string;
    cover_image: string;
    published_at: string;
    user_id: number;
    created_at: string;
    updated_at: string;
    subtitle: string;
    card_image: string;
    slug: string;
    tags: string[];
    url_post: string;
};
