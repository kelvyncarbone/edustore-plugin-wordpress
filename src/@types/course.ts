interface FinancialData {
    id: number;
    model_id: string;
    modality: string;
    description: string;
    price: string;
    status: string;
    financial_category_id: string;
    type_discount_automatic: null | string;
    price_discount_automatic: null | string;
    rules_discount_automatic: null | string;
    price_offer: null | string;
    init_date_offer: null | string;
    end_date_offer: null | string;
    max_installments_credit_card: string;
    free_installments: null | string;
    created_at: string;
    updated_at: string;
    split_id: null | string;
    sales_limit: number;
    interest_rate_credit_card: null | string;
    interest_rate_boleto: null | string;
    cash_discount: null | string;
    cash_discount_boleto: null | string;
    condition_id: null | string;
    config_tks_page: null | string;
    model_type: string;
    sales: string;
    cash_discount_type: string;
    cash_discount_type_boleto: string;
    checkout_mode: number;
    signature_period: string;
    condition_offer_id: null | string;
    ex_student_discount: string;
    ex_student_discount_type: string;
    courses_discount: null | string;
    end_data_discount: null | string;
    two_credit_card: string;
    two_card_min_value: null | string;
    cash_discount_pix: null | string;
    cash_discount_type_pix: string;
    style: null | string;
    hide_payment_methods: null | string;
    external_link: null | string;
    reenroll_discount: null | string;
    reenroll_discount_type: null | string;
    days_to_reenroll: number;
    deleted_at: null | string;
    original_amount: string;
    have_offer: boolean;
    condition: null | Condition;
    offer: null | string;
}

interface Installment {
    id: number;
    label: string;
    installment: string;
    amount: string;
    condition_detail_id: string;
    created_at: string;
    updated_at: string;
    status_id: string;
    amount_pagarme: number;
}

interface ConditionDetail {
    id: number;
    title: string;
    subtitle: string | null;
    condition_id: string;
    payment_method_id: string;
    created_at: string;
    updated_at: string;
    total_installments: string;
    status_id: number;
    installments: Installment[];
}

interface Condition {
    id: number;
    condition: string;
    created_at: string;
    updated_at: string;
    condition_details: ConditionDetail[];
}

interface PaymentMethod {
    condition: string;
    subtitle: null | string;
    total_installments: string;
    installments: Installment[];
    last_installment: Installment;
    first_installment: Installment;
}

interface Installment {
    label: string;
    installment: string;
    amount: string;
    amount_label: string;
    discount_amount: string;
    gross_amount: string;
    gross_amount_label: string;
    amount_pagarme: number;
}

interface Coupon {
    is_valid: boolean;
    code: null | string;
    coupon_message: null | string;
    discount: number;
    error: string;
    counter_date: null | string;
}

interface TraditionalBoleto {
    label: string;
    installment: number;
    amount: number;
    gross_amount: number;
    amount_pagarme: string;
    amount_label: string;
    gross_amount_label: string;
}

interface Pix {
    label: string;
    installment: number;
    amount: number;
    gross_amount: string;
    amount_pagarme: string;
    gross_amount_label: string;
    amount_label: string;
    payment_method: string;
}

interface SmallestInstallment {
    label: string;
    installment: number;
    amount: string;
    amount_label: string;
    gross_amount_label: string;
    gross_amount: string;
    amount_pagarme: string;
    payment_method: string;
}

export interface Billing {
    online: {
        amount: string;
        amount_label: string;
        amount_pagarme: number;
        discount_amount_label: string;
        amount_ex_student_discount: null | number;
        amount_reenroll_discount: null | number;
        cash_discount: string;
        cash_discount_boleto: string;
        cash_discount_pix: string;
        cash_discount_type: string;
        cash_discount_type_boleto: string;
        cash_discount_type_pix: string;
        init_date_offer: null | string;
        end_date_offer: null | string;
        have_offer: boolean;
        offer: null | string;
        price_offer: string;
        model_id: string;
        model_type: string;
        hide_payment_methods: null;
        style: {
            "border-color": string;
            "background-color": string;
        };
        signature_period: null | string;
        ex_student_accept_coupon: boolean;
        free_installments: string;
        interest_rate_credit_card: string;
        max_installments_credit_card: string;
        original_amount: string;
        original_amount_label: string;
        sales_limit: number;
        sales: number;
        checkout_mode: number;
        external_link: null | string;
        recurrence: boolean;
        has_recurrence: string[];
        is_available: boolean;
        status: string;
        config_tks_page: {
            data: {
                content: null | string;
                products: {
                    id: string;
                    product_type: string;
                }[];
            };
            view: string;
        };
        modality: string;
        description: string;
        two_card_min_value: number;
        two_card_max_value: number;
        days_to_reenroll: number;
        two_credit_card: string;
        can_reenroll: boolean;
        coupon_condition: null | string;
        coupon: Coupon;
        payment_methods: {
            credit_card: PaymentMethod[];
            boleto: PaymentMethod[];
            traditional_credit_card: {
                first_installment: Installment;
                last_installment: Installment;
                installments: Installment[];
            };
            traditional_boleto: TraditionalBoleto;
            pix: Pix;
        };
        smallest_installment: SmallestInstallment;
        first_installment: Installment;
    };
}

export interface CourseProps {
    id: number;
    course: string;
    subtitle: string;
    page_course_description: null | string;
    slug: string;
    created_at: string;
    updated_at: string;
    published_at: string;
    logo: string;
    player: string;
    type: string;
    featured: string;
    status: number;
    ead_start_at: null | string;
    presential_start_at: null | string;
    presential_end_at: null | string;
    contest_id: null | string;
    period: string;
    contract_id: null | string;
    occupation_area_id: string;
    order: null | string;
    can_download: string;
    workload: string;
    video: null | string;
    ead_intro: null | string;
    expiration_at: null | string;
    cover: null | string;
    prospect: null | string;
    methodology: null | string;
    form_id: null | string;
    optionals_sessions_amount: null | string;
    guide: null | string;
    schedule: null | string;
    certificate_example: null | string;
    course_product_id: null | string;
    ead_intro_enrolled: null | string;
    show_for_questions: number;
    has_comments: boolean;
    has_adaptive_learning: boolean;
    exam_id: null | string;
    certificate_id: null | string;
    has_study_planning: boolean;
    deleted_at: null | string;
    published_at_formated: string;
    ead_start_at_formated: null | string;
    presential_start_at_formated: null | string;
    presential_end_at_formated: null | string;
    type_to_human: string;
    url_sale: string;
    teachers: null | string;
    billing: Billing;
    clean_page_description: null | string;
    financial_data: FinancialData[];
    contest: null | string;
    area: {
        id: number,
        area: string,
        order: null
    }
}
