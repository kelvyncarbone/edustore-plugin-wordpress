import { Billing } from "./course";

export interface EbookProps {
    id: number;
    image: string;
    title: string;
    billing: Billing;
    url_sale: string;
    subtitle: string;
}