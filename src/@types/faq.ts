interface FAQSection {
    id: number;
    title: string;
    content: string;
    created_at: string;
    updated_at: string;
    faq_id: number;
}

export interface faqProps {
    id: number,
    title: string,
    faq_sections: FAQSection[];
}