export interface ExamsProps {
    id: number;
    title: string;
    url: string;
    expiration_at_formated?: string;
    cover?: string;
    total_questions: number;
}