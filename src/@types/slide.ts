export interface SlideProps {
    id: number;
    model_type: string;
    url: string | null;
    type_url: number;
    src: string;
    operator_id: number;
    created_at: string;
    updated_at: string;
    published_at: string;
    deleted_at: string | null;
    title: string;
    subtitle: string | null;
    model_id: number;
    button_text: string | null;
    order: number;
    src_small: string;
    src_medium: string;
};
