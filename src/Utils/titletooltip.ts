/**
 * By Yair Even Or
 * https://github.com/yairEO/title-tooltip
 */

export function titleTooltip(opts) {
    opts = Object.assign({
        delay: 250,
        id: 'titleTtip',
        onShow() { },
        onHide() { }
    }, opts || {})

    // do not continue if already exists
    if (opts.id in window) return

    var hoverTimeout, titledElm, mousePos: { x: number, y: number } = { x: 0, y: 0 }, widthElm = 0, heightTitled = 0, heightElm;

    // bind global event listeners
    document.addEventListener('mouseover', onOver)
    document.addEventListener('mouseout', onOut)

    // create & inject custom tooltip
    var tipElm = document.createElement('div')
    tipElm.id = opts.id
    document.body.append(tipElm)

    // event handlers
    function onOver(e) {
        titledElm = e.target.closest('[title]')
        document.removeEventListener('mousemove', onMove)

        if (titledElm && titledElm.title) {
            document.addEventListener('mousemove', onMove)
            hoverTimeout = setTimeout(() => {
                opts.onShow(titledElm, tipElm)
                tipElm.setAttribute('data-over', "true")
                setMousePos()

            }, opts.delay)
            tipElm.innerHTML = "<div class='" + opts.id + "__text" + "'>" + titledElm.title + "</div>"
            // save current title and empty the attribute. restore on mouse-out
            titledElm._entitled = titledElm.title
            titledElm.title = ''

            const elmStyle = getComputedStyle(tipElm);

            widthElm = parseFloat(elmStyle.width) / 2;
            heightElm = parseFloat(elmStyle.height);
            heightTitled = parseFloat(getComputedStyle(titledElm).height);
        }
        else
            onOut(e)
    }

    /**
     * On mouse out, revert back everything as it was beofre mouse over
     */
    function onOut(e) {

        clearTimeout(hoverTimeout)
        document.removeEventListener('mousemove', onMove)

        tipElm.removeAttribute('style')
        tipElm.removeAttribute('data-over')

        if (titledElm && titledElm._entitled)
            titledElm.title = titledElm._entitled

        opts.onHide(titledElm, tipElm)
        titledElm = null
    }

    function onMove(e) {
        mousePos = { x: e.clientX, y: e.clientY }
        requestAnimationFrame(() => setMousePos())
    }

    function setMousePos() {
        var rect = tipElm.getBoundingClientRect()
        tipElm.style.setProperty('--mouse-pos', (mousePos.x - rect.left).toString())
        tipElm.style.left = (mousePos.x - widthElm) + "px";

        let top = (mousePos.y + heightTitled);

        if ((top + heightElm) > window.innerHeight) {
            top = (mousePos.y - heightElm - heightTitled);

            tipElm.style.setProperty('--icon-top', heightElm + "px")
            tipElm.style.setProperty('--icon-transform', "scaleY(-1)")

        }

        tipElm.style.top = top + "px";
    }

    function destroy() {
        document.removeEventListener('mouseover', onOver)
        document.removeEventListener('mouseout', onOut)
        document.removeEventListener('mousemove', onMove)
        document.body.removeChild(tipElm)
    }

    return {
        onOver, onOut, tipElm, destroy
    }
}
