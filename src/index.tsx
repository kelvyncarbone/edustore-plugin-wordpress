//@ts-ignore
import { createRoot } from '@wordpress/element';
import React from 'react';
import { API } from "./Request/Api";
import { titleTooltip } from "./Utils/titletooltip";
import './index.css';

/**
 * Import the stylesheet for the plugin.
 */
document.addEventListener('DOMContentLoaded', () => {

    const api = (document.getElementById('edu-store-api') as HTMLInputElement)!.value;
    API.setBaseURL(api);

    /**
     * SHORTCODES
     */
    const courses = document.getElementById('edustore_courses');
    if (courses) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        API.setBaseURL(api);

        import('./Pages/Courses').then(({ Courses: CoursesPage }) => {
            const root = createRoot(courses);
            root.render(<CoursesPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const ebooks = document.getElementById('edustore_ebooks');
    if (ebooks) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        API.setBaseURL(api);

        import('./Pages/Ebooks').then(({ Ebooks: EbooksPages }) => {
            const root = createRoot(ebooks);
            root.render(<EbooksPages />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const exams = document.getElementById('edustore_exams');
    if (exams) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        API.setBaseURL(api);

        import('./Pages/Exams').then(({ Exams: ExamsPage }) => {
            const root = createRoot(exams);
            root.render(<ExamsPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const cursos = document.getElementById('edustore_cursos');
    if (cursos) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        API.setBaseURL(api);

        import('./Pages/Courses').then(({ Courses: CoursesPage }) => {
            const root = createRoot(cursos);
            root.render(<CoursesPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const faq = document.getElementById('edustore_faq');
    if (faq) {
        import('./Pages/Faq').then(({ Faq: FaqPage }) => {
            const root = createRoot(faq);
            root.render(<FaqPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const testimonials = document.getElementById('edustore_testimonials');
    if (testimonials) {
        import('./Pages/Testimonials').then(({ Testimonials: TestimonialsPage }) => {
            const root = createRoot(testimonials);
            root.render(<TestimonialsPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const depoimentos = document.getElementById('edustore_depoimentos');
    if (depoimentos) {
        import('./Pages/Testimonials').then(({ Testimonials: TestimonialsPage }) => {
            const root = createRoot(depoimentos);
            root.render(<TestimonialsPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
    }

    const blog = document.getElementById('edustore_blog');
    if (blog) {
        import('./Pages/Blog').then(({ Blog: BlogPage }) => {
            const root = createRoot(blog);
            root.render(<BlogPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
        // return;
    }

    const sliders = document.getElementById('edustore_sliders');
    if (sliders) {
        import('./Pages/Sliders').then(({ Sliders: SlidersPage }) => {
            const root = createRoot(sliders);
            root.render(<SlidersPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
        // return;
    }

    const popups = document.getElementById('edustore_popups');
    if (popups) {
        import('./Pages/Popups').then(({ Popups: PopupsPage }) => {
            const root = createRoot(popups);
            root.render(<PopupsPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
        // return;
    }


    /**
     * ADMIN
     */
    const admin = document.getElementById('edu-store-admin');
    if (admin) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        const nonce = (document.getElementById('edu-store-nonce') as HTMLInputElement).value;
        API.setBaseURL(api);
        API.setLogin(nonce);

        import('./Pages/Admin').then(({ Admin: AdminPage }) => {
            const root = createRoot(admin);
            root.render(<AdminPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
        return;
    }

    const styles = document.getElementById('edu-store-styles');
    if (styles) {
        const api = (document.getElementById('edu-store-api') as HTMLInputElement).value;
        API.setBaseURL(api);

        import('./Pages/Styles').then(({ Styles: StylesPage }) => {
            const root = createRoot(styles);
            root.render(<StylesPage />)
        });

        titleTooltip({
            onShow: function (reference, popper) {
            }
        })
        return;
    }
});
