import React from "react";
import { CourseProps } from "../@types/course";
import { Button, CustomFlowbiteTheme } from "flowbite-react";
import { AiOutlineLink } from "react-icons/ai";
import { CourseAttsProps } from "../Pages/Courses";
import { FaArrowCircleRight } from "react-icons/fa";
import { EbookProps } from "../@types/ebook";
import { ExamsProps } from "../@types/exams";

const linkButton: CustomFlowbiteTheme["button"] = {
    "base": "",
    "color": {
        "cyan": ""
    }
}

interface CourseCardProps {
    courseItem: ExamsProps,
    defaultCover: string,
    atts: CourseAttsProps,
    classes?: string
}

const ExamCard: React.FC<CourseCardProps> = ({ courseItem, atts, defaultCover, classes = '' }) => {

    return <div className={`course_container ${classes}`}>
        <div>
            <div className='group/cover course_cover_container'>
                <a href={courseItem.url + window.location.search} className='relative'>
                    <img className='w-full h-full' src={courseItem.cover ?? defaultCover} alt={courseItem.title} />
                    <div className='absolute invisible group-hover/cover:visible flex justify-center w-full bottom-0 h-10 p-2 bg-gray-500/25'>
                        <AiOutlineLink size={20} color='white' />
                    </div>
                </a>
            </div>
            <div className='course_info_container'>
                <h2 className='course_title'>{courseItem.title}</h2>
                <div className='border-t w-full my-0.5 h-1' />
                <div className='flex w-full mb-0.5 justify-between'>
                    {courseItem.total_questions ? <span className='px-2 py-0.5 bg-[--edustore-secondary-color] rounded text-white text-sm'>{courseItem.total_questions === 1 ? `${courseItem.total_questions} Questão` : `${courseItem.total_questions} Questões`}</span> : <></>}
                    {courseItem.expiration_at_formated && <span className='px-2 py-0.5 bg-[--edustore-secondary-color] rounded text-white text-sm'>Expira em: {courseItem.expiration_at_formated}</span>}
                </div>
                {/* {courseItem.expiration_at_formated && <span className='course_subtitle'><span>Expira em: {courseItem.expiration_at_formated}</span></span>} */}
                <div className='course_see_container'>
                    <Button color='cyan' theme={linkButton} href={courseItem.url + window.location.search} className='course_see flex items-center'><span className='mr-2'>Conhecer Simulado</span> <FaArrowCircleRight size={20} /></Button>
                </div>
            </div>
        </div>
    </div>
}

export { ExamCard }