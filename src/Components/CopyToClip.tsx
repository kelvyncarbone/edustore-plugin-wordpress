import React, { useRef } from "react";
import { IoMdCopy } from "react-icons/io";
import { toast } from "react-toastify";

const CopyToClip: React.FC = () => {
    const ref = useRef<HTMLSpanElement>(null);

    const handleCopy = () => {
        const code = ((ref.current?.parentElement?.nextElementSibling as HTMLInputElement)?.value);

        if (code) {
            navigator.clipboard.writeText(code).then(() => toast('Shortcode copiado', {
                type: 'success',
                position: 'bottom-center'
            }))
        }
    }

    return <span className="h-6 w-6 cursor-pointer hover:text-[#2271b1]" onClick={() => { handleCopy() }} ref={ref}><IoMdCopy className="hover:text-[#2271b1] h-full w-full" /></span>
}

export { CopyToClip }