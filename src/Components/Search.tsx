import { Button, CustomFlowbiteTheme, TextInput } from "flowbite-react";
import React, { useState } from "react";
import { AiOutlineSearch, AiOutlineClose } from "react-icons/ai";

interface SearchProps {
    handleSearch: (e: string) => void,
    doRequestSearch: () => void,
    placeholder: string
}

const theme: CustomFlowbiteTheme["textInput"] = {
    "base": "flex",
    "addon": "inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-200 px-3 text-sm text-gray-900 dark:border-gray-600 dark:bg-gray-600 dark:text-gray-400",
    "field": {
        "base": "relative w-full",
        "icon": {
            "base": "pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3",
            "svg": "h-5 w-5 text-gray-500 dark:text-gray-400"
        },
        "rightIcon": {
            "base": "z-10 absolute inset-y-0 right-0 flex items-center pr-3",
            "svg": "h-5 w-5 text-gray-500 dark:text-gray-400"
        },
    }
}

const Search: React.FC<SearchProps> = ({ handleSearch, doRequestSearch, placeholder }) => {
    const [query, setQuery] = useState<string>("");

    const Close = () => <span onClick={() => {
        handleSearch('');
        setQuery('');
    }} className="cursor-pointer"><AiOutlineClose /></span>

    return <>
        <TextInput value={query} theme={theme} rightIcon={query.length ? Close : undefined} onKeyDown={(event) => event.key.toLowerCase() === 'enter' ? doRequestSearch() : ''} onChange={(e) => {
            setQuery(e.target.value)
            handleSearch(e.target.value)
        }} placeholder={placeholder} className='w-full sm:w-[65%] sm:mr-2' />
        <Button onClick={() => doRequestSearch()} className='h-full flex w-full sm:w-[35%] mt-2 sm:mt-0 items-center bg-[--edustore-primary-color] enabled:hover:bg-[--edustore-secondary-color]'><span className='mr-2'>Pesquisar</span><AiOutlineSearch size={20} /></Button>
    </>
}

export { Search }