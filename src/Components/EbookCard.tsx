import React from "react";
import { CourseProps } from "../@types/course";
import { Button, CustomFlowbiteTheme } from "flowbite-react";
import { AiOutlineLink } from "react-icons/ai";
import { CourseAttsProps } from "../Pages/Courses";
import { FaArrowCircleRight } from "react-icons/fa";
import { EbookProps } from "../@types/ebook";

const linkButton: CustomFlowbiteTheme["button"] = {
    "base": "",
    "color": {
        "cyan": ""
    }
}


interface CourseCardProps {
    courseItem: EbookProps,
    atts: CourseAttsProps,
    classes?: string
}

const EbookCard: React.FC<CourseCardProps> = ({ courseItem, atts, classes = '' }) => {


    let { original_amount, have_offer, price_offer, amount: price, first_installment } = courseItem.billing?.online;

    if (have_offer && price_offer) {
    }
    else if (original_amount !== price) {
        have_offer = true;
        price_offer = price;
    }
    else if (Number(first_installment.amount) < Number(price)) {
        price_offer = first_installment.amount;
        have_offer = true;
    }

    return <div className={`course_container ${classes}`}>
        <div>
            <div className='group/cover course_cover_container'>
                <a href={courseItem.url_sale + window.location.search} className='relative'>
                    {have_offer && <span className='absolute left-1 top-1 z-10 uppercase text-[12px] px-2 py-0.5 bg-[--edustore-primary-color] rounded text-white text-sm'>Oferta</span>}

                    <img className='w-full h-full' src={courseItem.image ?? ''} alt={courseItem.title} />
                    <div className='absolute invisible group-hover/cover:visible flex justify-center w-full bottom-0 h-10 p-2 bg-gray-500/25'>
                        <AiOutlineLink size={20} color='white' />
                    </div>
                </a>
            </div>
            <div className='course_info_container'>
                <h2 className='course_title'>{courseItem.title}</h2>
                <div className='border-t w-full my-0.5 h-1' />
                {courseItem.subtitle && <span className='course_subtitle'><span>{courseItem.subtitle}</span></span>}
                {atts.config.show_price === "y" &&
                    <div className='course_price_container'>
                        {
                            have_offer ?
                                <div className='course_price_discount_container relative'>
                                    <span className='course_price_discount'>R${original_amount}</span>
                                    {
                                        Number(price_offer) > 0 ?
                                            <span className='course_price'>R${price_offer}</span> :
                                            <>
                                                <span className='course_price'>R${price_offer}</span>
                                                <span className='course_free'>Grátis</span>
                                            </>
                                    }
                                </div> : <>
                                    {
                                        Number(original_amount) > 0 ?
                                            <span className='course_price'>R${original_amount}</span> :
                                            <div className='flex flex-col w-full items-center '>
                                                <span className='course_price mb-2'>R${original_amount}</span>
                                                <span className='course_free'>Grátis</span>
                                            </div>
                                    }</>
                        }
                    </div>
                }
                <div className='course_see_container'>
                    <Button color='cyan' theme={linkButton} href={courseItem.url_sale + window.location.search} className='course_see flex items-center'><span className='mr-2'>Conhecer Material</span> <FaArrowCircleRight size={20} /></Button>
                </div>
            </div>
        </div>
    </div>
}

export { EbookCard }