<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://edustore.online
 * @since      1.0.0
 *
 * @package    Edu_Store
 * @subpackage Edu_Store/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Edu_Store
 * @subpackage Edu_Store/admin
 * @author     EduStore <contato@edustore.online>
 */
class Edu_Store_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Player_Hub_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Player_Hub_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$screen_id = get_current_screen()->id;

		if (strpos($screen_id, $this->plugin_name) !== false)
			wp_enqueue_style($this->plugin_name, plugin_dir_url(dirname(__FILE__)) . 'assets/js/index.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Player_Hub_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Player_Hub_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$screen_id = get_current_screen()->id;

		if (strpos($screen_id, $this->plugin_name) !== false)
			wp_enqueue_script($this->plugin_name, plugin_dir_url(dirname(__FILE__)) . 'assets/js/edu-store.js',  array('react', 'react-dom', 'wp-element'), $this->version, false);
	}

	public function admin_head()
	{
		$screen_id = get_current_screen()->id;

		if (strpos($screen_id, $this->plugin_name) !== false) {
			remove_all_actions('admin_notices');
		}
	}

	public function add_menus()
	{
		add_menu_page('EduStore', 'EduStore', 'manage_options', $this->plugin_name, function () {
			$this->output_menu('admin');
		}, 'dashicons-welcome-learn-more');

		add_submenu_page($this->plugin_name, 'Estilos', 'Estilos', 'manage_options', $this->plugin_name . "-styles", function () {
			$this->output_menu('styles');
		});
	}

	public function output_menu($page)
	{
		require_once 'partials/edu-store-admin-display.php';
	}
}
