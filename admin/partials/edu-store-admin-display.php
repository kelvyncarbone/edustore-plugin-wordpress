<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://edustore.online
 * @since      1.0.0
 *
 * @package    Edu_Store
 * @subpackage Edu_Store/admin/partials
 */
?>

<style>
    #wpcontent {
        background-color: white;
    }
</style>

<input type="hidden" id="edu-store-api" value="<?php echo rest_url('edu-store/v1'); ?>" />
<input type="hidden" id="edu-store-nonce" value="<?php echo wp_create_nonce('wp_rest'); ?>" />

<div id="<?php echo "{$this->plugin_name}-" . esc_attr($page); ?>">
    <div style="display:flex; flex-direction:column; background-color: #f1f2f3; width: 100%; height:100vh; align-items:center; justify-content:center">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(241, 242, 243); display: block; shape-rendering: auto;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" r="32" stroke-width="8" stroke="#1d0e0b" stroke-dasharray="50.26548245743669 50.26548245743669" fill="none" stroke-linecap="round">
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform>
            </circle>
        </svg>
    </div>
</div>