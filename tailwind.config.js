/** @type {import('tailwindcss').Config} */
module.exports = {
  corePlugins: {
    preflight: false,
  },
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
    'node_modules/flowbite-react/lib/esm/**/*.js',
  ],
  theme: {
    extend: {

    },
    fontFamily: {
      'sans': ['Roboto', 'ui-sans-serif', 'sans-serif']
    }
  },
  plugins: [require('flowbite/plugin')],
  important: true
}

