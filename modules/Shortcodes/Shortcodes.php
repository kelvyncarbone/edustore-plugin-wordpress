<?php

namespace EduStore\Shortcodes;

use EduStore\Traits\Singleton;

class Shortcodes
{
    use Singleton;

    const TAGS = [
        'edustore_cursos',
        'edustore_courses',
        'edustore_faq',
        'edustore_testimonials',
        'edustore_depoimentos',
        'edustore_sliders',
        'edustore_popups',
        'edustore_blog',
        'edustore_ebooks',
        'edustore_exams',
    ];

    // private $tag = 'edu_store_cursos';

    protected function __construct()
    {
        add_action("init", function () {
            foreach (self::TAGS as $tag) {
                add_shortcode($tag, function ($atts) use ($tag) {
                    return $this->output($atts, $tag);
                });
            }
        });
    }

    public function output($atts, $tag)
    {
        wp_enqueue_style("edu-store");
        wp_enqueue_script("edu-store");

        $atts = shortcode_atts([
            'per_page' => 25,
            'show_html' => 'yes',
            'time_interval' => 5000,
            'frequency' => 1,
            'theme' => 'cards',
            'title' => 'Depoimentos',
            'show_title' => 'y',
            'show_price' => 'y',
            'assets_url' => plugins_url('assets', dirname(__FILE__, 2))
        ], $atts,  $tag);


        $old_atts = $atts;
        $atts = [];
        $atts['config'] = $old_atts;

        ob_start();
?>
        <input type="hidden" id="edu-store-api" value="<?php echo rest_url('edu-store/v1'); ?>" />
        <input type="hidden" id="<?php echo esc_attr($tag) ?>_atts" value="<?php echo esc_attr(json_encode($atts)); ?>" />

        <div class="edustore_app" id="<?php echo esc_attr($tag) ?>">
            <?php if ($atts['config']['show_html'] === 'yes') : ?>

                <div style="display:flex; flex-direction:column; background-color: transparent; width: 100%; height:100vh; align-items:center; justify-content:center">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgba(241, 242, 243, 0); display: block; shape-rendering: auto;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                        <circle cx="50" cy="50" r="32" stroke-width="8" stroke="var(--edustore-primary-color)" stroke-dasharray="50.26548245743669 50.26548245743669" fill="none" stroke-linecap="round">
                            <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" keyTimes="0;1" values="0 50 50;360 50 50"></animateTransform>
                        </circle>
                    </svg>
                </div>
            <?php endif; ?>

        </div>
<?php
        return ob_get_clean();
    }
}
