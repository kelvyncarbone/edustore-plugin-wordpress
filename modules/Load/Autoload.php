<?php

namespace EduStore\Load;

use EduStore\Traits\Singleton;

require_once plugin_dir_path(dirname(__FILE__)) . "Traits/Singleton.php";

class Autoload
{

    use Singleton;

    private function __construct()
    {
        spl_autoload_register(array($this, 'load'));
    }

    private function load($className)
    {
        if (strpos($className, 'EduStore') !== false) {
            $extension = '.php';
            $source = plugin_dir_path(dirname(__FILE__)) .  ltrim(str_replace(['EduStore', '\\'], ['', DIRECTORY_SEPARATOR],  $className . $extension), '\\/');

            if (file_exists($source))
                require_once($source);
        }
    }
}


Autoload::get_instance();
