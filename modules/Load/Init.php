<?php

namespace EduStore\Load;

use EduStore\API\Routes;
use EduStore\Shortcodes\Shortcodes;
use EduStore\Traits\Singleton;
use EduStore\Updater\Updater;

require_once 'Autoload.php';


class Init
{
    use Singleton;

    private $config = null;

    private function __construct()
    {
        Routes::get_instance();
        Shortcodes::get_instance();

        if ($this->config === null) {
            $this->config =  get_option('edu-store-config', [
                'corPrimaria' => '#ffffff',
                'corSecundaria' => '#ffffff',
            ]);
        }

        add_action('wp_head', function () {
            $edustore_options = $this->config;

?>
            <style id="edustore-styles-codes">
                :root {
                    --edustore-primary-color: <?php echo $edustore_options['corPrimaria']; ?>;
                    --swiper-pagination-color: <?php echo $edustore_options['corPrimaria']; ?>;
                    --edustore-primary-color-transparent: <?php echo $this->hexToRGBA($edustore_options['corPrimaria']); ?>;
                    --edustore-secondary-color: <?php echo $edustore_options['corSecundaria']; ?>;
                    --pagination-color: <?php echo $edustore_options['corSecundaria']; ?>;
                }

                <?php
                $saved = get_option('edustore_shortcodes_styles');

                if (!empty($saved) && $saved['useCustom'])
                    echo $saved['style']
                ?>
            </style>

        <?php
        });

        add_action('admin_head', function () {
            $edustore_options = $this->config;

        ?>
            <style id="edustore-styles-codes">
                :root {
                    --edustore-primary-color: <?php echo $edustore_options['corPrimaria']; ?>;
                    --swiper-pagination-color: <?php echo $edustore_options['corPrimaria']; ?>;
                    --edustore-primary-color-transparent: <?php echo $this->hexToRGBA($edustore_options['corPrimaria']); ?>;
                    --edustore-secondary-color: <?php echo $edustore_options['corSecundaria']; ?>;
                    --pagination-color: <?php echo $edustore_options['corSecundaria']; ?>;
                }
            </style>

<?php
        });

        add_action('wp_footer', function () {

            if (empty($this->config['popup'])) return;

            $frequency = $this->config['popupFrequency'] ?? 1;

            $has_displayed = $_COOKIE['edustore_show_popup_count'] ?? 0;

            if ($has_displayed < $frequency || $frequency === -1) {
                $timeInterval = $this->config['popupTimeInterval'] ?? 5000;
                echo do_shortcode('[edustore_popups showHtml="not" frequency="' . $frequency . '" time_interval="' . $timeInterval . '" ]');
            }
        });

        add_action('init', function () {
            if (is_admin()) { // note the use of is_admin() to double check that this is happening in the admin
                new Updater(EDU_STORE_UPDATE_CONFIG);
            }
        });
    }

    private function hexToRGBA($hex, $alpha = 0.5)
    {
        $hex = str_replace('#', '', $hex);

        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));

        return "rgba($r, $g, $b, $alpha)";
    }
}

Init::get_instance();
