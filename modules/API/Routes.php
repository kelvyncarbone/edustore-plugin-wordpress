<?php

namespace EduStore\API;

use EduStore\Traits\Singleton;

class Routes
{

    use Singleton;

    const CACHE_KEYS = [
        'courses' => 'edustore_courses',
        'faq' => 'edustore_faq',
        'blog' => 'edustore_blog',
        'testimonials' => 'edustore_testimonials',
        'popups' => 'edustore_popups',
        'sliders' => 'edustore_sliders',
        'depoimentos' => 'edustore_depoimentos',
        'ebooks' => 'edustore_ebooks',
        'exams' => 'edustore_exams',
    ];

    private static $API_URL = null;
    private static $API_USER = null;
    private static $API_SECRET = null;

    private $base_rest = 'edu-store/v1';

    public function __construct()
    {
        $data =  get_option('edu-store-config', ['urlAPI' => 'https://painel.edustore.online/api/v1/']);

        self::$API_URL = trailingslashit($data['urlAPI']);
        self::$API_USER = $data['usuario'] ?? '';
        self::$API_SECRET = $data['senha'] ?? '';

        add_action('rest_api_init', array($this, 'register_rest_routes'));
    }

    public function register_rest_routes()
    {

        //TODO: Register Routes with loop
        // foreach (self::CACHE_KEYS as $route => $cache_key) {
        //     register_rest_route($this->base_rest, "/$route", array(
        //         'methods'  => 'GET',
        //         'callback' = [$this, 'register_route', [$route, $cache_key, ]],
        //         'permission_callback' => '__return_true',
        //     ));
        // }

        register_rest_route($this->base_rest, '/styles', array(
            'methods'  => 'GET',
            'callback' => function (\WP_REST_Request $request) {

                $saved = get_option('edustore_shortcodes_styles');

                $type = $request->get_param('type');

                if (empty($saved) || $type === 'default') {
                    $styles = file_get_contents(plugin_dir_path(dirname(__FILE__, 2)) .  'assets/js/index.css');
                    return rest_ensure_response(['data' => $styles, 'isCustom' => false, 'status' => 'ok', 'message' => '']);
                }

                return rest_ensure_response(['data' => $saved['style'], 'isCustom' => $saved['useCustom'], 'status' => 'ok', 'message' => '']);
            },
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/styles', array(
            'methods'  => 'POST',
            'callback' => function (\WP_REST_Request $request) {
                $styles = $request->get_param('code');
                $isCustom = $request->get_param('isCustom');

                update_option('edustore_shortcodes_styles', ['style' => $styles, 'useCustom' => $isCustom]);

                return rest_ensure_response(['data' => $styles, 'isCustom' => $isCustom, 'status' => 'ok', 'message' => 'Configurações Salvas com sucesso']);
            },
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/courses', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_courses'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/ebooks', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_ebooks'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/exams', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_exams'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/faq', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_faq'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/testimonials', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_testimonials'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/blog', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_blog'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/popups', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_popups'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/popup-view', array(
            'methods'  => 'POST',
            'callback' => array($this, 'set_popup_view'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/sliders', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_sliders'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/lives', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_lives'),
            'permission_callback' => '__return_true',
        ));

        register_rest_route($this->base_rest, '/config', array(
            'methods'  => 'POST',
            'callback' => array($this, 'update_config'),
            'permission_callback' => function () {
                return current_user_can('manage_options');
            },
        ));

        register_rest_route($this->base_rest, '/config', array(
            'methods'  => 'GET',
            'callback' => array($this, 'get_config'),
            'permission_callback' => function () {
                return current_user_can('manage_options');
            },
        ));
    }

    public function get_courses(\WP_REST_Request $request)
    {
        $per_page = $request->get_param('per_page');
        $page = $request->get_param('page');

        $url = "courses?billing=true&per_page=$per_page&page=$page";

        $category = $request->get_param('currentCategory');

        if ($category !== 'all') {
            $url .= "&area=$category";
        }

        $search = $request->get_param('s');

        if (!empty($search)) {

            $response = self::remote_post('courses/search?billing=true', json_encode(
                [
                    'query' => $search,
                ]
            ));

            if (!is_wp_error($response) && is_array($response)) {

                if (isset($response['data']['data']))
                    $response = $response['data'];

                $response['status'] = 'ok';
            }

            return rest_ensure_response($response);
        }

        $response = self::remote_get($url);

        $categories = self::remote_get('occupation-areas');

        $cache_key = self::CACHE_KEYS['courses'] . "_$url";

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        $categories_data = array_merge([['id' => 'all', 'area' => 'Todos os Cursos']], $categories['data']);
        $response['categories']['data'] = $categories_data;

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);

        return rest_ensure_response($response);
    }

    public function get_ebooks(\WP_REST_Request $request)
    {
        $per_page = $request->get_param('per_page');
        $page = $request->get_param('page');

        $url = "ebooks?billing=true&per_page=$per_page&page=$page";

        $category = $request->get_param('currentCategory');

        if ($category !== 'all') {
            $url .= "&area=$category";
        }

        $search = $request->get_param('s');

        if (!empty($search)) {

            $url .= "&query=$search";
        }

        $response = self::remote_get($url);

        $cache_key = self::CACHE_KEYS['ebooks'] . "_$url";

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        } elseif (is_array($response)) {

            if (isset($response['data']['data']))
                $response = $response['data'];

            $response['status'] = 'ok';

            set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);
        }

        return rest_ensure_response($response);
    }

    public function get_exams(\WP_REST_Request $request)
    {
        $per_page = $request->get_param('per_page');
        $page = $request->get_param('page');

        $url = "public-exams?billing=true&per_page=$per_page&page=$page";

        $category = $request->get_param('currentCategory');

        if ($category !== 'all') {
            $url .= "&area=$category";
        }

        $search = $request->get_param('s');

        if (!empty($search)) {

            $url .= "&query=$search";

            // $response = self::remote_post('courses/search?billing=true', json_encode(
            //     [
            //         'query' => $search,
            //     ]
            // ));

            // if (!is_wp_error($response) && is_array($response)) {

            //     if (isset($response['data']['data']))
            //         $response = $response['data'];

            //     $response['status'] = 'ok';
            // }

            // return rest_ensure_response($response);
        }

        $response = self::remote_get($url);

        $cache_key = self::CACHE_KEYS['exams'] . "_$url";

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        } elseif (is_array($response)) {

            if (isset($response['data']['data']))
                $response = $response['data'];

            $response['status'] = 'ok';

            set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);
        }

        return rest_ensure_response($response);
    }

    public function get_faq(\WP_REST_Request $request)
    {
        $response = self::remote_get('faq');

        $cache_key = self::CACHE_KEYS['faq'];

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);


        return rest_ensure_response($response);
    }

    public function set_popup_view(\WP_REST_Request $request)
    {
        $count = isset($_COOKIE['edustore_show_popup_count']) ? intval($_COOKIE['edustore_show_popup_count']) : 0;

        $count++;

        setcookie('edustore_show_popup_count', $count, time() + (DAY_IN_SECONDS), '/', parse_url(get_site_url(), PHP_URL_HOST));

        return [$count];
    }

    public function get_testimonials(\WP_REST_Request $request)
    {
        $response = self::remote_get('testimonials');

        $cache_key = self::CACHE_KEYS['testimonials'];

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);

        return rest_ensure_response($response);
    }

    public function get_blog(\WP_REST_Request $request)
    {
        $response = self::remote_get('blog/posts');

        $cache_key = self::CACHE_KEYS['blog'];

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['data'] = $response['data']['data'];

        $response['status'] = 'ok';

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);

        return rest_ensure_response($response);
    }

    public function get_popups(\WP_REST_Request $request)
    {
        $response = self::remote_get('popups');

        $cache_key = self::CACHE_KEYS['popups'];
        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);


        return rest_ensure_response($response);
    }

    public function get_sliders(\WP_REST_Request $request)
    {
        $response = self::remote_get('sliders');

        $cache_key = self::CACHE_KEYS['sliders'];

        if (($cached = get_transient($cache_key)) !== false)
            return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        set_transient($cache_key, $response, 0.05 * HOUR_IN_SECONDS);

        return rest_ensure_response($response);
    }

    public function get_lives(\WP_REST_Request $request)
    {
        $response = self::remote_get('lives');

        // if (($cached = get_transient('edustore_courses')) !== false)
        //     return $cached;

        if (is_wp_error($response)) {
            error_log(var_export($response, 1));

            return rest_ensure_response(['status' => 'err', 'message' => 'Ocorreu um erro na api ao tentar listar os cursos.']);
        }

        $response['status'] = 'ok';

        // set_transient('edustore_courses', $response, 0.05 * HOUR_IN_SECONDS);


        return rest_ensure_response($response);
    }

    public function update_config(\WP_REST_Request $request)
    {

        $data = $request->get_json_params();

        $old_data =  get_option('edu-store-config', ['urlAPI' => '']);

        if ($old_data['urlAPI'] !== $data['urlAPI']) {
            foreach (self::CACHE_KEYS as $cache_key) {
                delete_transient($cache_key);
            }
        }

        update_option('edu-store-config', $data);

        return rest_ensure_response(['message' => 'Configurações Salvas Com Sucesso', 'status' => 'ok']);
    }

    public function get_config(\WP_REST_Request $request)
    {

        return rest_ensure_response([
            'message' => 'Configurações Salvas Com Sucesso', 'status' => 'ok', 'data' =>
            get_option('edu-store-config', [
                'usuario' => '',
                'senha' => '',
                'urlVitrine' => '',
                'urlPainel' => '',
                'urlSala' => '',
                'corPrimaria' => '#ffffff',
                'corSecundaria' => '#ffffff',
                'popup' => false
            ])
        ]);
    }

    private function auth()
    {
        $config =  get_option('edu-store-config', [
            'usuario' => '',
            'senha' => ''
        ]);

        $res = self::remote_post('auth/login', json_encode([
            "email" => $config['usuario'],
            "password" => $config['senha'],
            "device_name" => 'site'
        ]));

        if (is_array($res) && !empty($res['access_token'])) {
            set_transient('edustore_auth_token', $res['access_token'], HOUR_IN_SECONDS);

            return $res['access_token'];
        }
    }

    public static function remote_post($route, $params = [])
    {
        $response = wp_remote_post(self::$API_URL . $route, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'body' => ($params)
        ]);

        if (!is_wp_error($response)) {
            return json_decode(wp_remote_retrieve_body($response), 1);
        }

        return $response;
    }

    public static function remote_get($route, $params = [])
    {
        $response = wp_remote_get(self::$API_URL . $route, [
            'headers' => self::request_headers()
        ]);

        if (!is_wp_error($response)) {
            return json_decode(wp_remote_retrieve_body($response), 1);
        }

        return $response;
    }

    public static function request_headers()
    {
        if (($access_token = get_transient('edustore_auth_token')) === false) {
            $access_token = self::get_instance()->auth();
        }

        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $access_token,
            // 'ApiDoc' => true,
        ];
    }
}
