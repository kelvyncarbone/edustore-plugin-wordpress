<?php

namespace EduStore\Traits;

trait SetPropsOnConstruct
{

    public function __construct($data)
    {
        foreach ($data as $k => $d)
            if (property_exists($this, $k))
                $this->$k = $d;
    }
}
