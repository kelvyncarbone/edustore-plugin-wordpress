<?php

namespace EduStore\Traits;

trait Singleton
{
    protected static $_instance = null;

    public static function get_instance()
    {
        if (self::$_instance ===  null)
            self::$_instance = new self();

        return self::$_instance;
    }
}
