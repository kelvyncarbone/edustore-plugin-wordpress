[2/1 18:54] Job WORKANA: https://carranzacursos.com.br/
[2/1 18:56] Job WORKANA: Documentação EduStore: https://edustore.online/docs/
[2/1 19:08] Job WORKANA: *PLUGIN [CONFIGURAÇÃO]*

- Usuário
- Senha
- Endpoint da API
- URL da vitrine
- URL do painel
- URL da sala
- Cor primária
- Cor secundária

*Shortcodes*
- [-] Cursos (Filtro de busca, estado e área de atuação)
- Materiais/Ebooks (filtro de busca)
- Lives (filtro de busca)
- FAQ
- Testimonials
- Popups
- Slides/Sliders

*Configurações dos shortcodes*
- CSS customizado
- Quantidade de registros