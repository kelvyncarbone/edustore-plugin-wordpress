<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://edustore.online
 * @since             1.0.0
 * @package           Edu_Store
 *
 * @wordpress-plugin
 * Plugin Name:       EduStore
 * Plugin URI:        https://edustore.online
 * Description:       Plugin para integração com a plataforma EduStore
 * Version:           1.0.3
 * Author:            EduStore
 * Author URI:        https://edustore.online/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       edu-store
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

define('EDU_STORE_UPDATE_CONFIG', array(
	'slug' => plugin_basename(__FILE__),
	'proper_folder_name' => dirname(plugin_basename(__FILE__)),
	'api_url' => 'https://gitlab.com/api/v4/projects/kelvyncarbone%2Fedustore-plugin-wordpress',
	'raw_url' => 'https://gitlab.com/kelvyncarbone/edustore-plugin-wordpress/-/raw/main',
	'github_url' => 'https://gitlab.com/kelvyncarbone/edustore-plugin-wordpress',
	'zip_url' => 'https://gitlab.com/kelvyncarbone/edustore-plugin-wordpress/-/archive/main/edustore-plugin-wordpress-master.zip',
	'sslverify' => true,
	'requires' => '3.0',
	'tested' => '3.3',
	'readme' => 'README.txt',
	'access_token' => '',
));

define('EDU_STORE_DEBUG', false);
define('EDU_STORE_PLUGIN', 'edu-store');
define('EDU_STORE_FORCE_UPDATE', false);

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
if (EDU_STORE_DEBUG)
	define('EDU_STORE_VERSION', uniqid());
else
	define('EDU_STORE_VERSION', '1.0.3');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-edu-store-activator.php
 */
function activate_edu_store()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-edu-store-activator.php';
	Edu_Store_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-edu-store-deactivator.php
 */
function deactivate_edu_store()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-edu-store-deactivator.php';
	Edu_Store_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_edu_store');
register_deactivation_hook(__FILE__, 'deactivate_edu_store');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-edu-store.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_edu_store()
{

	$plugin = new Edu_Store();
	$plugin->run();
}
run_edu_store();
